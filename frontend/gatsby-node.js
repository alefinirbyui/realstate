const path = require(`path`)

exports.createPages = async ({ graphql, actions }) => {

  const { data } = await graphql(`
  query properties {
    allStrapiProperty {
      nodes {
        slug
      }
    }
  }
  
  `)

  data.allStrapiProperty.nodes.forEach(node => {
    actions.createPage({
      path: '/properties/'+ node.slug,
      component: path.resolve('./src/templates/property-details.js'),
      context: { slug: node.slug }
    })
  })
}