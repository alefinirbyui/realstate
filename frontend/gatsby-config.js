/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
    `gatsby-transformer-remark`,
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: "gatsby-source-strapi",
      options: {
        apiURL: "http://localhost:1337",
        collectionTypes: [
          "property"
        ],
        queryLimit: 1000,
      },
    },
    'gatsby-plugin-image',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`,
      },
    },    
  ],
  siteMetadata: {
    title: 'Sweet Home',
    description: 'Real Estate',
    copyright: 'This website is copyright 2021 Sweet Home',
    contact: 'alefinir@sweethome.com',
  },  
}
