import React from "react"
import Layout from "../components/layout"
//import { GatsbyImage, getImage } from "gatsby-plugin-image"
//import * as styles from "../styles/project-details.module.css"
import { FaDollarSign, FaLocationArrow, FaFileContract, FaWarehouse, FaWhatsapp } from "react-icons/fa";
import { graphql, Link } from "gatsby"
import { newspaper, btn, contactus } from "../styles/property-details.module.css"

export default function propertyDetails({ data }){
    console.log("esto es data:", data.strapiProperty.image1["localFile"].childImageSharp.gatsbyImageData.images.fallback.src)
    const { description, ptype, cost, location, transaction } = data.strapiProperty
    const { header } = data.strapiProperty
    const { url } = data.strapiProperty.image1["localFile"]
  return (
    <Layout>
        <div class={newspaper}>
        <img src={url} alt="decorative"/>
                <h2>{header}</h2>
                <p>{description}</p>
                <br/>
                <h3>Extra info:</h3>
                <FaDollarSign fill="#15425C" size="25px"/>Cost: {cost}<br/>
                <FaLocationArrow fill="#15425C" size="25px"/>Location: {location}<br/>
                <FaFileContract fill="#15425C" size="25px"/>Transaction: {transaction}<br/>
                <FaWarehouse fill="#15425C" size="25px"/>Property Type: {ptype}<br/>
                <br/>
                <br/>
                <h3>Contact us</h3>
                <div className={contactus}>
                  <Link className={btn} to="https://api.whatsapp.com/send?phone=59897085094"><FaWhatsapp fill="white" size="24px"/></Link>
                </div>  
        </div>

    </Layout>
  )

}
//            <GatsbyImage alt="" image={data.strapiProperty.image1["localFile"].childImageSharp.gatsbyImageData} />


export const query = graphql`
query ProjectDetails($slug: String) {
    strapiProperty(slug: {eq: $slug}) {
      cost
      created_at
      description
      header
      header2
      id
      location
      ptype
      slug
      transaction
      image1 {
        localFile {
          url
          childImageSharp {
            gatsbyImageData(
                layout: FULL_WIDTH
                placeholder: BLURRED
                width: 424
                height: 212
            )
          }
        }
      }
    }
  }  
`

  /*
  query ProjectDetails($slug: String) {
  strapiProperty(slug: {eq: $slug}) {
    cost
    created_at
    description
    header
    header2
    id
    location
    ptype
    slug
    transaction
    image1 {
      localFile {
        childImageSharp {
          gatsbyImageData(
            layout: FIXED
            placeholder: DOMINANT_COLOR
            width: 10
            height: 10
          )
        }
      }
    }
  }
}


               <div class={dtables}>
                <table class={center}>
                    <tr>
                        <th>Cost</th>
                        <td>{cost}</td>
                    </tr>
                    <tr>
                        <th>Location</th>
                        <td>{location}</td>
                    </tr>
                    <tr>
                        <th>Transaction</th>
                        <td>{transaction}</td>
                    </tr>
                    <tr>
                        <th>Property Type</th>
                        <td>{ptype}</td>
                    </tr>
                </table>
                </div>


        <div className={html} dangerouslySetInnerHTML={{__html: html}} />
  */

        // line 39                <img src={url} alt="decorative"/>