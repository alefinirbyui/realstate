import React from 'react';
import { useForm, ValidationError } from '@formspree/react';
import { btn } from "../styles/sell.module.css"

export default function ContactForm() {
  const [state, handleSubmit] = useForm("mpzbapke");

  if (state.succeeded) {
    return <p>Thanks for your submission!</p>;
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>First name</label>
      <input id="firstname" type="text" name="firstName" />
      <ValidationError prefix="Firstname" field="firstName" errors={state.errors} />

      <label>Last name</label>
      <input id="lastname" type="text" name="lastName" />
      <ValidationError prefix="Lastname" field="lastName" errors={state.errors} />

      <label htmlFor="email">Email Address</label>
      <input id="email" type="email" name="email" />
      <ValidationError prefix="Email" field="email" errors={state.errors} />

      <label htmlFor="message">Message</label>
      <textarea id="message" name="message"></textarea>
      <ValidationError prefix="Message" field="message" errors={state.errors} />

      <button className={ btn } type="submit" disabled={state.submitting}>
        Submit
      </button>

      <ValidationError errors={state.errors} />

    </form>
  );
}
