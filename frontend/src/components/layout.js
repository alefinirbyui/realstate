import React from 'react'
import Navbar from './Navbar'
import "../styles/global.css"
import { FaLinkedin, FaFacebookSquare, FaRegEnvelope } from "react-icons/fa";
import { Link } from 'gatsby'

export default function layout({ children }) {
    return (
        <div className="layout">
            <header>
                <Navbar />
            </header>
            <div className="content">
                { children }
            </div>
            <footer>
                <p>Copyright 2021 Sweet Home<br/>
                <Link to="/"><FaFacebookSquare fill="#15425C" size="30px" />  <FaLinkedin fill="#15425C" size="30px" />  <FaRegEnvelope fill="#15425C" size="30px" /></Link></p>  
            </footer>
        </div>
    )
}