import React from 'react'
import Layout from "../components/layout"

//import Carousel from 'react-bootstrap/Carousel';
//import 'bootstrap/dist/css/bootstrap.min.css';
import { container } from "../styles/sell.module.css"
import ContactForm from "../components/contact-form";

export default function Sell({ data }) {
    return (
        <Layout>
          <section>
          <div className={container}>  
            <h1>Contact with Sweet Home Real Estate</h1> 
            <ContactForm />     
          </div>
        </section>
        </Layout>
    )
}

//          <Img fluid={data.file.childImageSharp.fluid} />