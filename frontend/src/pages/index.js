import { graphql, Link } from "gatsby"
import React from "react"
import Layout from "../components/layout"
//import '../styles/home.module.css'
import { header, btn, properties2, onebox } from "../styles/home.module.css"
import Img from "gatsby-image"
import { FaFire, FaChartBar, FaBuilding, FaCity, FaCommentDollar} from "react-icons/fa";

export default function Home({ data }) {
  return (
    <Layout>
            <section className={header}>
              <div>
                <h2>Sweet Home</h2>
                <h3>Real Estate</h3>
                <p>We have what you are looking for.</p>
                <p>We have a large portfolio of properties, we invite you to see some of the most outstanding</p>
                <Link className={btn} to="/properties/highlighted">Featured Properties</Link>
              </div>
              <Img fluid={data.file.childImageSharp.fluid} />
            </section>
            <div className={ properties2 }>
              <div className={ onebox }>              
                <h3><FaCity fill="#15425C" size="75px" /></h3>
                <h3>Buy a Home</h3>
                <br></br>
                <p>Find your place with an immersive photo experience and the most listings, including things you wont find anywhere else.</p>
                <h3><Link className={btn} to="/properties/buy">Search Homes</Link></h3>
              </div>
              <div className={ onebox }>              
                <h3><FaCommentDollar fill="#15425C" size="75px" /></h3>
                <h3>Sell a Property</h3>
                <br></br>
                <p>No matter what path you take to sell your home, we can help you navigate a succesful sale. We can help you without any doubt.</p>
                <h3><Link className={btn} to="/sell">See your options</Link></h3>
              </div>
              <div className={ onebox }>              
                <h3><FaBuilding fill="#15425C" size="75px" /></h3>
                <h3>Rent a Home</h3>
                <br></br>
                <p>We are creating a seamless online experiencie - from shopping on the largest rentarl network, to applying, to paying rent.</p>
                <h3><Link className={btn} to="/properties/rent">Find Rentals</Link></h3>
              </div>
            </div>
    </Layout>
  )
}
export const query = graphql`
  query Banner {
    file(relativePath: {eq: "banner.png"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`