import React from 'react'
import Layout from "../../components/layout"
//import '../../styles/projects.module.css'
import { property, properties2, backdark } from "../../styles/properties.module.css"
import { Link, graphql } from 'gatsby'
//import Img from "gatsby-image"
import { GatsbyImage } from 'gatsby-plugin-image'

export default function farms({ data }) {
  
  const properties = data.allStrapiProperty.nodes
  //console.log(properties)/* hasta aca */
  //if (!posts) return null
  //if (!Array.isArray(posts)) return null
  return (
    <Layout>
      <div className={ property }>
        <h2>Farms</h2>
        <h3>Farms are Available Now!</h3>
        <div className={ properties2 }>
          {properties.map(property => (
            <Link to={`/properties/${property.slug}`}>
              <div>
                <GatsbyImage alt="" image={property.image1["localFile"].childImageSharp.gatsbyImageData} />
                <h3>{property.header}</h3>
                <div>{property.header2}</div>
                <p>$ {property.cost}</p>
                <div className={ backdark }>View Details</div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </Layout>
  );
}

// export page query
export const query = graphql`
query farmsQuery {
  allStrapiProperty(filter: { ptype: { eq: "land" } }) {
    nodes {
      header
      header2
      slug
      published_at(formatString: "MMMM Do, YYYY")
      image1 {
        localFile {
          childImageSharp {
            gatsbyImageData(
              layout: FULL_WIDTH
              placeholder: BLURRED
              width: 424
              height: 212
        		)
          }
        }
      }
      image2 {
        localFile {
          childImageSharp {
            gatsbyImageData(
              layout: FULL_WIDTH
              placeholder: BLURRED
              width: 424
              height: 212
        		)
          }
        }
      }
      image3 {
        localFile {
          childImageSharp {
            gatsbyImageData(
              layout: FULL_WIDTH
              placeholder: BLURRED
              width: 424
              height: 212
        		)
          }
        }
      }      
      description
      cost
      transaction
    }
  }
}
`
//                <GatsbyImage alt="" image={property.image1.localFile.childImageSharp.gatsbyImageData} />
//                <h4>{property.image1.}</h4>