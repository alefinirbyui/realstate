import React from 'react'
import Layout from "../components/layout"
import { newspaper } from "../styles/oneproperty.module.css"

//import { FaBeer } from "@react-icons/all-files/fa/FaBeer";
//import { FaFacebookF } from "@react-icons/all-files/fa/FaFacebookF";
//import { FaApple } from "@react-icons/all-files/fa/FaApple";
//import { FaDollarSign } from "@react-icons/all-files/fa/FaDollarSign";
import { FaDollarSign, FaApple, FaFacebookF, FaBeer} from "react-icons/fa";
import Img from 'gatsby-image';
import Carousel from 'react-bootstrap/Carousel';


export default function About() {
    return (
        
        <Layout>


        <div class={newspaper}>
            <figure>
                <img src="https://web-warrior-gatsby-by-ibrahim.netlify.app/static/04db8de731b8f81d3cc0d02f1306b484/274a6/gaming.png" alt="test image"/>
            </figure>

            <h2>Caracteristics</h2>
            <p> aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
            <FaApple fill="#15425C" size="25px"/>Caracteristics<br/>
            <FaFacebookF fill="#15425C" size="25px"/>Caracteristics<br/>
            <FaDollarSign fill="#15425C" size="25px"/>Caracteristics<br/>
            <FaBeer fill="#15425C" size="25px" />Caracteristics<br/>
            </div>


            <Carousel variant="dark">
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://web-warrior-gatsby-by-ibrahim.netlify.app/static/04db8de731b8f81d3cc0d02f1306b484/274a6/gaming.png"
      alt="First slide"
    />
    <Carousel.Caption>
      <h5>First slide label</h5>
      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://web-warrior-gatsby-by-ibrahim.netlify.app/static/04db8de731b8f81d3cc0d02f1306b484/274a6/gaming.png"
      alt="Second slide"
    />
    <Carousel.Caption>
      <h5>Second slide label</h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://web-warrior-gatsby-by-ibrahim.netlify.app/static/04db8de731b8f81d3cc0d02f1306b484/274a6/gaming.png"
      alt="Third slide"
    />
    <Carousel.Caption>
      <h5>Third slide label</h5>
      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
            
        </Layout>
    )
}
//                <Img fluid={pic.node.childImageSharp.fluid}/>