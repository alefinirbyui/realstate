import React from 'react'
import Layout from "../components/layout"
import { graphql } from 'gatsby'
import Img from "gatsby-image"
//import Carousel from 'react-bootstrap/Carousel';
//import 'bootstrap/dist/css/bootstrap.min.css';
import { header } from "../styles/about.module.css"


export default function About({ data }) {
    return (
        <Layout>
          <section className={header}>
          <div>  
            <h2>About Sweet Home Real Estate</h2>
            <p>Our real estate company is a leader in the regional market; we have at least 90% of the values ​​in real estate transactions, a team of 15 highly specialized agents in real estate brokerage and we are perceived as the company that has the highest management efficiency with a score of 99% in customer recommendations according to market studies and surveys carried out periodically and made available by the company.</p>
            <h2>Vision</h2>
            <p>Implement a real estate management system (similar to that of leading real estate companies in the world) based on constant improvement processes that in turn are aligned in developing strategies that considerably increase our market share and are committed to the objectives set.</p>
            <h2>Mission</h2>
            <p>Achieve only win-win businesses in which all parties are 100% satisfied with the results achieved while maintaining a focus on the total quality of the added value delivered in each business.</p>
           </div>
           <div>
            <Img fluid={data.file.childImageSharp.fluid} />
            <h2>Roberto Rototo - CEO</h2>
           </div>
        </section>
        </Layout>
    )
}
export const query = graphql`
  query President {
    file(relativePath: {eq: "president2.jpeg"}) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

//          <Img fluid={data.file.childImageSharp.fluid} />